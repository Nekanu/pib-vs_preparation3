package com.nekanu.pibvs_preparation3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PibVsPreparation3Application {

    public static void main(String[] args) {
        SpringApplication.run(PibVsPreparation3Application.class, args);
    }

}
