package com.nekanu.pibvs_preparation3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PrimeNumbers {
    private int previousPrimeLimit = 2;
    private final ArrayList<Boolean> primeSieve;

    public PrimeNumbers() {
        primeSieve = new ArrayList<>(4);

        primeSieve.add(false); // 0 is not prime
        primeSieve.add(false); // 1 is not prime
        primeSieve.add(true); // 2 is prime
        primeSieve.add(true); // 3 is prime
    }

    public synchronized List<Integer> sieve(int exclusiveLimit) {

        // Check if the limit is less than the cached limit
        if (exclusiveLimit <= previousPrimeLimit) {
            // If so, generate the list from the cached sieve
            return getPrimeNumbersFromSieve(exclusiveLimit);
        }

        // If not, extend the cached sieve
        for (int i = previousPrimeLimit; i < exclusiveLimit; i++) {
            primeSieve.add(true);
        }
        previousPrimeLimit = exclusiveLimit;

        // Sieve of Eratosthenes
        for (int i = 2; i*i < exclusiveLimit; i++) {
            if (primeSieve.get(i)) {
                for (int j = i * i; j < exclusiveLimit; j += i) {
                    primeSieve.set(j, false);
                }
            }
        }

        return getPrimeNumbersFromSieve(exclusiveLimit);
    }

    private List<Integer> getPrimeNumbersFromSieve(int limit) {
        return IntStream.range(0, limit).parallel().filter(primeSieve::get).boxed().toList();
    }
}
