package com.nekanu.pibvs_preparation3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class PrimeNumbersController {
    private final PrimeNumbers primeNumbers = new PrimeNumbers();

    @GetMapping("/prime-numbers-array")
    public List<Integer> primeNumbersArray(@RequestParam int limit) {
        return primeNumbers.sieve(limit);
    }

    @GetMapping("/prime-numbers-string")
    public String primeNumbersString(@RequestParam int limit) {
        return primeNumbers.sieve(limit).toString();
    }

    @GetMapping("/prime-numbers-structure")
    public List<Integer> primeNumbersStructure(@RequestParam int limit) {
        return primeNumbers.sieve(limit);
    }
}
